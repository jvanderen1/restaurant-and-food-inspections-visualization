module.exports = {
    "collectCoverageFrom": [
        "src/js/**/*.js"
    ],
    "transform": {
        "^.+\\.(j|t)s?$": "babel-jest"
    },
}
