// Internal
// --------

function countOccurrences(arr, comparator) {
    return arr.reduce((a, v) => (
        comparator(v) ? a + 1 : a
    ), 0);
}

// Exports
// -------

export default countOccurrences;
