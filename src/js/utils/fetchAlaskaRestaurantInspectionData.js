// Internal
// --------

function fetchAlaskaRestaurantInspectionData() {
    return Promise.all([
        import(/* webpackChunkName: "ariBusinessName" */ '../../data/alaskaRestaurantInspectionData/business_name.json'),
        import(/* webpackChunkName: "ariBusinessAddress" */ '../../data/alaskaRestaurantInspectionData/business_address.json'),
        import(/* webpackChunkName: "ariBusinessCity" */ '../../data/alaskaRestaurantInspectionData/business_city.json'),
        import(/* webpackChunkName: "ariBusinessState" */ '../../data/alaskaRestaurantInspectionData/business_state.json'),
        import(/* webpackChunkName: "ariBusinessPostalCode" */ '../../data/alaskaRestaurantInspectionData/business_postal_code.json'),
        import(/* webpackChunkName: "ariBusinessPhoneNumber" */ '../../data/alaskaRestaurantInspectionData/business_phone_number.json'),
        import(/* webpackChunkName: "ariInspectionDate" */ '../../data/alaskaRestaurantInspectionData/inspection_date.json'),
        import(/* webpackChunkName: "ariInspectionScore" */ '../../data/alaskaRestaurantInspectionData/inspection_score.json'),
        import(/* webpackChunkName: "ariInspectionDescription" */ '../../data/alaskaRestaurantInspectionData/inspection_description.json'),
        import(/* webpackChunkName: "ariInspectionType" */ '../../data/alaskaRestaurantInspectionData/inspection_type.json'),
        import(/* webpackChunkName: "ariViolationDescription" */ '../../data/alaskaRestaurantInspectionData/violation_description.json'),
        import(/* webpackChunkName: "ariViolationCode" */ '../../data/alaskaRestaurantInspectionData/violation_code.json'),
        import(/* webpackChunkName: "ariLatitude" */ '../../data/alaskaRestaurantInspectionData/latitude.json'),
        import(/* webpackChunkName: "ariLongitude" */ '../../data/alaskaRestaurantInspectionData/longitude.json')
    ]).then(modules => {
        return {
            business_name: modules[0].default,
            business_address: modules[1].default,
            business_city: modules[2].default,
            business_state: modules[3].default,
            business_postal_code: modules[4].default,
            business_phone_number: modules[5].default,
            inspection_date: modules[6].default,
            inspection_score: modules[7].default,
            inspection_description: modules[8].default,
            inspection_type: modules[9].default,
            violation_description: modules[10].default,
            violation_code: modules[11].default,
            latitude: modules[12].default,
            longitude: modules[13].default
        };
    });
}

// Exports
// -------

export default fetchAlaskaRestaurantInspectionData;
