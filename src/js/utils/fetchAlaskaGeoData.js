// Internal
// --------

function fetchAlaskaGeoData() {
    return import(/* webpackChunkName: "alaskaGeoData" */ '../../data/alaskaGeoData.json')
        .then(module => module.default);
}

// Exports
// -------

export default fetchAlaskaGeoData;
