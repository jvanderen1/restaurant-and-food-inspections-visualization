// Imports
// -------

// Libraries
import React from 'react';

// Internal
// --------

const tooltipContextValues = {
    display: 'block',
    top: '0px',
    left: '0px',
    innerText: ''
};

const TooltipContext = React.createContext('ToolTipContext');

// Exports
// -------

export {
    tooltipContextValues
};

export default TooltipContext;
