// Imports
// -------

// Libraries
import React from 'react';

// Internal
// --------

const narrativeContextValues = true;

const ShowNarrativeContext = React.createContext('ShowNarrativeContext');

// Exports
// -------

export {
    narrativeContextValues
};

export default ShowNarrativeContext;
