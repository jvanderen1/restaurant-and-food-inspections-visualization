// Imports
// -------

// Libraries
import React, { lazy, Suspense, useRef, useState, useEffect, useCallback } from 'react';
import * as d3 from 'd3';
// Components
import Loading from './Loading';
// Lazy Components
const AlaskaMap = lazy(() => import(/* webpackChunkName: "AlaskaMap" */ './AlaskaMap'));
const RestaurantInspections = lazy(() => import(/* webpackChunkName: "RestaurantInspections" */ './RestaurantInspections'));

// Internal
// --------

//
// Components

function AlaskaRestaurantInspections({ alaskaGeoDataReader, alaskaRestaurantInspectionDataReader, selectedTime, setSelectedItem }) {
    //
    // References

    const svgRef = useRef(null);

    //
    // State

    const [gTransform, setGTransform] = useState('');

    //
    // Instance variables

    const alaskaGeoData = alaskaGeoDataReader();
    const alaskaRestaurantInspectionData = alaskaRestaurantInspectionDataReader();

    const projection = d3.geoMercator()
        .scale(20000)
        .center([-149.086129, 61.155235])
        .translate([0, 0]);

    const geoGenerator = d3.geoPath()
        .projection(projection)
        .pointRadius(2);

    //
    // Instance methods

    const zoomHandler = useCallback(() => {
        const {
            x,
            y,
            k
        } = d3.event.transform;
        setGTransform(`translate(${x}, ${y}) scale(${k})`);
    }, []);

    //
    // OnMount

    useEffect(() => {
        const svg = d3.select(svgRef.current);
        const zoom = d3.zoom()
            .on('zoom', zoomHandler);
        svg.call(zoom);
        console.log("Set?")
    }, []);

    //
    // Render

    return (
        <svg ref={ svgRef } className='alaskaMapData' viewBox='-500 -500 1000 1000'>
            <AlaskaMap alaskaGeoData={ alaskaGeoData } geoGenerator={ geoGenerator } transform={ gTransform } />
            <Suspense fallback={ <Loading /> }>
                <RestaurantInspections
                    alaskaRestaurantInspectionData={ alaskaRestaurantInspectionData }
                    geoGenerator={ geoGenerator }
                    selectedTime={ selectedTime }
                    setSelectedItem={ setSelectedItem }
                    transform={ gTransform }
                />
            </Suspense>
        </svg>
    );
}

// Exports
// -------

export default AlaskaRestaurantInspections;
