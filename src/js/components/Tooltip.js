// Imports
// -------

// Libraries
import React, { useContext, memo } from 'react';
// Context
import TooltipContext from '../context/TooltipContext';

// Internal
// --------

//
// Helpers

function compareProps(prevProps, nextProps) {
    return true;
}

//
// Components

const Tooltip = memo(() => {
    //
    // Context

    const {
        tooltipValues: {
            innerText,
            ...style
        }
    } = useContext(TooltipContext);

    //
    // Render

    return (
        <div id='tooltip' className='tooltip bg-orange-100 p-1 text-xs rounded' style={ style }>
            { innerText }
        </div>
    );
}, compareProps);

// Exports
// -------

export default Tooltip;
