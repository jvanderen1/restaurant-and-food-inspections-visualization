// Imports
// -------

// Libraries
import React, { memo } from 'react';
// Images
import anchorageAlaskaImage from '../../img/anchorage-alaska.png';

// Internal
// --------

//
// Helpers

function compareProps(prevProps, nextProps) {
    return true;
}

//
// Components

const TitleBar = memo(() => {
    //
    // Render

    return (
        <div className='titleBar bg-yellow-200'>
            <img className="h-full px-5 py-2 rounded-md" src={ anchorageAlaskaImage } alt="Anchorage, AK" />
            <p className='self-center'>
                <span
                    className='font-title font-light text-pink-600 text-base sm:text-2xl md:text-3xl lg:text-4xl xl:text-5xl'>Restaurant
                    Inspections</span>
                <span
                    className='font-light font-bold text-pink-400 m-2 text-base sm:text-2xl md:text-3xl lg:text-4xl xl:text-5xl'>in</span>
                <span
                    className='font-title font-light text-pink-600 text-base sm:text-2xl md:text-3xl lg:text-4xl xl:text-5xl'>Anchorage,
                    AK</span>
            </p>
        </div>
    );
}, compareProps);

// Exports
// -------

export default TitleBar;
