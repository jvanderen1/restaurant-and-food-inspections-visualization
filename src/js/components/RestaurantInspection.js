// Imports
// -------

// Libraries
import React, { memo } from 'react';

// Internal
// --------

//
// Helpers

function compareProps(prevProps, nextProps) {
    return prevProps.dataIndex === nextProps.dataIndex;
}

//
// Components

const RestaurantInspection = memo(({ d, dataIndex }) => {
    //
    // Render

    return (
        <path d={ d } data-index={ dataIndex } />
    );
}, compareProps);

// Exports
// -------

export default RestaurantInspection;
