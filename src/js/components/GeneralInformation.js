// Imports
// -------

// Libraries
import React from 'react';
// Images
import anchorageAlaskaImage from '../../img/anchorage-alaska.jpg';
// Utils
import countOccurrences from '../utils/countOccurrences';

// Internal
// --------

// TODO: Major improvement can be made by tracking last "currentPosition"
//  (see RestaurantInspections.js)
function getCurrentPosition(arr, selectedTime) {
    let currentPosition = 0;

    while (arr[currentPosition] <= selectedTime) {
        currentPosition += 1;
    }

    return currentPosition - 1;
}

//
// Helpers

function getGeneralInformation(alaskaRestaurantInspections, selectedTime) {
    const firstDate = new Date(+alaskaRestaurantInspections.inspection_date[0]);
    const displayFirstDate = firstDate.toLocaleString().split(',')[0];

    const date = new Date(+selectedTime);
    const displayDate = date.toLocaleString().split(',')[0];

    const currentPosition = getCurrentPosition(alaskaRestaurantInspections.inspection_date, selectedTime);
    const numberOfViolations = countOccurrences(alaskaRestaurantInspections.violation_code.slice(0, currentPosition + 1), (candidate) => (+candidate !== -1));

    return {
        firstDate: displayFirstDate,
        date: displayDate,
        violations: numberOfViolations,
        total_inspections: currentPosition + 1
    };
}

//
// Components

const GeneralInformation = ({ selectedTime, alaskaRestaurantInspectionDataReader }) => {
    //
    // Render

    const alaskaRestaurantInspectionData = alaskaRestaurantInspectionDataReader();

    const generalInformation = getGeneralInformation(alaskaRestaurantInspectionData, selectedTime);

    return (
        <div className="card bg-yellow-100">
            <div className="px-6 py-4">
                <div className="font-title font-bold text-pink-800 text-base text-xl mb-2">
                    { generalInformation.firstDate + ' - ' + generalInformation.date }
                </div>
                <div className="flex mb-1">
                    <div className="w-2/3">
                        <p className="font-body font-bold sm:text-s md:text-m text-base text-pink-600 block pr-4">
                            Inspections
                        </p>
                    </div>
                    <div className="w-1/3">
                        <p className="font-body sm:text-s md:text-m text-base text-pink-600">
                            { generalInformation.total_inspections }
                        </p>
                    </div>
                </div>
                <div className="flex mb-1">
                    <div className="w-2/3">
                        <p className="font-body font-bold text-m text-base text-pink-600 block pr-4">
                            Violations
                        </p>
                    </div>
                    <div className="w-1/3">
                        <p className="font-body text-m text-base text-pink-600">
                            { generalInformation.violations }
                        </p>
                    </div>
                </div>
            </div>
        </div>
    );
};

//
// PropTypes


// Exports
// -------

export default GeneralInformation;
