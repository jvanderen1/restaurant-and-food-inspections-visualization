// Imports
// -------

// Libraries
import React, { memo, useRef, useEffect } from 'react';
import * as d3 from 'd3';
import * as topojson from 'topojson-client';

// Internal
// --------

//
// Helpers

function compareProps(prevProps, nextProps) {
    return prevProps.transform === nextProps.transform;
}

//
// Components

const AlaskaMap = memo(({ alaskaGeoData, geoGenerator, transform }) => {
    //
    // References

    const gRef = useRef(null);

    //
    // OnMount

    useEffect(() => {
        const g = d3.select(gRef.current);

        // Add land
        const land = topojson.feature(alaskaGeoData, alaskaGeoData.objects.cb_2015_alaska_county_20m);

        g.append('path')
            .datum(land)
            .attr("class", "land")
            .attr("d", geoGenerator);

        // Add borders
        const borders = topojson.mesh(alaskaGeoData, alaskaGeoData.objects.cb_2015_alaska_county_20m, (a, b) => a !== b);

        g.append('path')
            .datum(borders)
            .attr("class", "borders")
            .attr("d", geoGenerator);
    }, []);

    //
    // Render

    return (
        <g ref={ gRef } transform={ transform }/>
    );
}, compareProps);

//
// PropTypes


// Exports
// -------

export default AlaskaMap;
