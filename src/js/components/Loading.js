// Imports
// -------

// Libraries
import React from 'react';

// Internal
// --------

//
// Components

function Loading() {
    //
    // Render

    return (
        <div className='loader'/>
    );
}

// Exports
// -------

export default Loading;
