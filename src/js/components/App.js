// Imports
// -------

// Libraries
import React, { lazy, Suspense, useState, useCallback } from 'react';
import { useAsyncResource } from 'use-async-resource';
// Components
import Loading from './Loading';
// Utils
import fetchAlaskaGeoData from '../utils/fetchAlaskaGeoData';
import fetchAlaskaRestaurantInspectionData from '../utils/fetchAlaskaRestaurantInspectionData';
// Components
import TitleBar from './TitleBar';
import Tooltip from './Tooltip';
// Context
import TooltipContext, { tooltipContextValues } from '../context/TooltipContext';
import ShowNarrativeContext from '../context/ShowNarrativeContext';
// Lazy Components
const GeneralInformation = lazy(() => import(/* webpackChunkName: "GeneralInformation" */ './GeneralInformation'));
const Narrative = lazy(() => import(/* webpackChunkName: "Narrative" */ './Narrative'));
const SpecificInformation = lazy(() => import(/* webpackChunkName: "SpecificInformation" */ './SpecificInformation'));
const AlaskaRestaurantInspections = lazy(() => import(/* webpackChunkName: "AlaskaRestaurantInspections" */ './AlaskaRestaurantInspections'));
const TimeRangeSlider = lazy(() => import(/* webpackChunkName: "TimeRangeSlider" */ './TimeRangeSlider'));

// Internal
// --------

const urlParams = new URLSearchParams(window.location.search);
let narrativeParam = urlParams.get('narrative');

//
// Components

function App() {
    //
    // Async Resources

    const [alaskaGeoDataReader] = useAsyncResource(fetchAlaskaGeoData, []);
    const [alaskaRestaurantInspectionDataReader] = useAsyncResource(fetchAlaskaRestaurantInspectionData, []);

    //
    // State

    const [selectedTime, setSelectedTime] = useState(null);
    const [selectedItem, setSelectedItem] = useState(0);
    const [tooltipValues, setTooltipValues] = useState(tooltipContextValues);
    const [showNarrative, setShowNarrative] = useState(!!narrativeParam);

    //
    // Instance Methods

    const handleSetTooltipValues = useCallback((obj) => {
        setTooltipValues(obj);
    }, []);

    const handleSetShowNarrative = useCallback((obj) => {
        setShowNarrative(obj);
    }, []);

    //
    // Render

    return (
        <ShowNarrativeContext.Provider
            value={ { showNarrative, setShowNarrative: handleSetShowNarrative } }>
            <TooltipContext.Provider
                value={ { tooltipValues, setTooltipValues: handleSetTooltipValues } }>
                <div className='w-full h-full'>
                    <TitleBar />
                    <Suspense fallback={ <Loading /> }>
                        <div className='body'>
                            <div className='flex-1' style={ { minWidth: '250px' } }>
                                <GeneralInformation selectedTime={ selectedTime }
                                                    alaskaRestaurantInspectionDataReader={ alaskaRestaurantInspectionDataReader } />
                                <SpecificInformation
                                    alaskaRestaurantInspectionDataReader={ alaskaRestaurantInspectionDataReader }
                                    selectedItem={ selectedItem } />
                            </div>
                            <div className='flex-auto px-2' style={ { width: '500px' } }>
                                <AlaskaRestaurantInspections
                                    alaskaGeoDataReader={ alaskaGeoDataReader }
                                    alaskaRestaurantInspectionDataReader={ alaskaRestaurantInspectionDataReader }
                                    selectedTime={ selectedTime }
                                    setSelectedItem={ setSelectedItem } />
                                <Suspense fallback={ <Loading /> }>
                                    <TimeRangeSlider
                                        selectedTime={ selectedTime }
                                        setSelectedTime={ setSelectedTime }
                                        alaskaRestaurantInspectionDataReader={ alaskaRestaurantInspectionDataReader } />
                                </Suspense>
                            </div>
                            {
                                showNarrative && (
                                    <div className='flex-1'
                                         style={ { minWidth: '250px' } }>
                                        <Narrative setSelectedItem={ setSelectedItem }
                                                   setSelectedTime={ setSelectedTime } />
                                    </div>
                                )
                            }
                        </div>
                    </Suspense>
                </div>
                <Tooltip />
            </TooltipContext.Provider>
        </ShowNarrativeContext.Provider>
    );
}

// Exports
// -------

export default App;
