// Imports
// -------

// Libraries
import React, { useEffect, useState, useCallback, useContext, useRef, memo } from 'react';
import * as d3 from 'd3';
// Context
import TooltipContext from '../context/TooltipContext';
import ShowNarrativeContext from '../context/ShowNarrativeContext';

// Internal
// --------

//
// Helpers

function getCurrentPosition({ inspection_date }, currentPosition, selectedTime) {
    const direction = (inspection_date[currentPosition] <= selectedTime) ? 1 : -1;
    const length = inspection_date.length;

    while (direction * inspection_date[currentPosition] <= direction * selectedTime && currentPosition < length) {
        currentPosition += direction;
    }

    return (currentPosition - direction);
}

function getInspections({ latitude, longitude, inspection_date }, currentPosition) {
    return latitude.slice(0, currentPosition + 1).map((lat, i) => ({
            'type': 'Feature',
            'geometry': {
                'type': 'Point',
                'coordinates': [longitude[i], lat]
            }
        })
    );
}

function compareProps(prevProps, nextProps) {
    return prevProps.transform === nextProps.transform && prevProps.selectedTime === nextProps.selectedTime;
}

//
// Components

const RestaurantInspections = memo(({ alaskaRestaurantInspectionData, geoGenerator, selectedTime, setSelectedItem, transform }) => {
    //
    // References

    const gRef = useRef(null);

    //
    // State

    const [currentPosition, setCurrentPosition] = useState(0);

    //
    // Context

    const {
        showNarrative
    } = useContext(ShowNarrativeContext);

    const {
        setTooltipValues
    } = useContext(TooltipContext);

    //
    // Instance Methods

    const handleMouseOver = useCallback((event) => {
        const dataIndex = +event.target.getAttribute('data-index');
        setSelectedItem(dataIndex);

        const {
            clientX: x,
            clientY: y
        } = event;

        const latitude = alaskaRestaurantInspectionData.latitude[dataIndex];
        const longitude = alaskaRestaurantInspectionData.longitude[dataIndex];

        setTooltipValues({
            display: 'block',
            top: `${ y + 5 }px`,
            left: `${ x + 5 }px`,
            innerText: `(${ latitude }, ${ longitude })`
        });
    }, []);

    const handleMouseOut = useCallback((event) => {
        const dataIndex = +event.target.getAttribute('data-index');
        setSelectedItem(dataIndex);

        setTooltipValues({
            display: 'none'
        });
    }, []);

    //
    // ShouldComponentUpdate

    useEffect(() => {
        setCurrentPosition((oldPosition) => getCurrentPosition(alaskaRestaurantInspectionData, oldPosition, selectedTime));
    }, [selectedTime]);

    useEffect(() => {
        const g = d3.select(gRef.current);

        // Add inspections
        const inspections = getInspections(alaskaRestaurantInspectionData, currentPosition);

        const paths = g.selectAll('path')
            .data(inspections);

        paths.enter()
            .append('path')
            .attr('data-index', (d, i) => i)
            .attr('d', geoGenerator);

        paths.exit()
            .remove();
    }, [currentPosition]);

    const onMouseOver = showNarrative ? () => null : handleMouseOver;
    const onMouseOut = showNarrative ? () => null : handleMouseOut;

    return (
        <g ref={ gRef } onMouseOver={ onMouseOver } onMouseOut={ onMouseOut } transform={ transform }/>
    );
}, compareProps);

// Exports
// -------

export default RestaurantInspections;
