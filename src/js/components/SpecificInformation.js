// Imports
// -------

// Libraries
import React from 'react';

// Internal
// --------


//
// Helpers

function getLocationInformation({ business_city, business_postal_code, business_state, latitude, longitude }, selectedItem) {
    if (selectedItem === null) {
        return null;
    }

    const locationName = `${ business_city[selectedItem] }, ${ business_state[selectedItem] } ${ business_postal_code[selectedItem] }`;
    const coordinates = `(${ latitude[selectedItem] }, ${ longitude[selectedItem] })`;

    return {
        locationName,
        coordinates
    };
}

function getFormattedPhoneNumber({ business_phone_number }, selectedItem) {
    const selectedPhoneNumber = business_phone_number[selectedItem];
    const cleaned = ('' + selectedPhoneNumber).replace(/\D/g, '');
    const match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/);

    if (match) {
        const intlCode = (match[1] ? '+1 ' : '');
        return [intlCode, '(', match[2], ') ', match[3], '-', match[4]].join('');
    }

    return null;
}

function getViolationInformation({ violation_code, violation_description }, selectedItem) {
    if (violation_code[selectedItem] === -1) {
        return {
            violation: false
        };
    }

    return {
        violation: true,
        code: violation_code[selectedItem],
        description: violation_description[selectedItem]
    };
}

function getDate({ inspection_date }, selectedItem) {
    const date = new Date(inspection_date[selectedItem]);
    return date.toLocaleString().split(',')[0];
}

//
// Components

// TODO: Feature idea: embed Google image of address
const SpecificInformation = ({ alaskaRestaurantInspectionDataReader, selectedItem }) => {
    //
    // Render

    const alaskaRestaurantInspectionData = alaskaRestaurantInspectionDataReader();

    const location = getLocationInformation(alaskaRestaurantInspectionData, selectedItem);
    const phoneNumber = getFormattedPhoneNumber(alaskaRestaurantInspectionData, selectedItem);
    const violationInformation = getViolationInformation(alaskaRestaurantInspectionData, selectedItem);
    const date = getDate(alaskaRestaurantInspectionData, selectedItem);

    return (
        <div className="card bg-yellow-100">
            <div className="px-6 py-2">
                <div className="font-title font-bold text-pink-800 text-base text-2xl">
                    { alaskaRestaurantInspectionData.business_name[selectedItem] }
                </div>
                <div className="font-body text-pink-600 text-base text-l">
                    { location.locationName }
                </div>
                <div className="font-body text-pink-600 text-base text-l">
                    { phoneNumber }
                </div>
            </div>
            <div className="px-6 py-2">
                <div className="font-title font-bold text-pink-800 text-base text-2xl">
                    Inspection
                </div>
                <div className="font-body text-pink-600 text-base text-l">
                    { date }
                </div>
                {
                    (alaskaRestaurantInspectionData.inspection_score[selectedItem] !== -1) && (
                        <div className="flex">
                            <div className="w-2/3">
                                <p className="font-body font-bold text-m text-base text-pink-600 block pr-4">
                                    Score
                                </p>
                            </div>
                            <div className="w-1/3">
                                <p className="font-body text-m text-base text-pink-600">
                                    {  alaskaRestaurantInspectionData.inspection_score[selectedItem] }
                                </p>
                            </div>
                        </div>
                    )
                }
            </div>
            {
                violationInformation.violation && (
                    <div className="px-6 py-4">
                        <div className="font-title font-bold text-pink-800 text-base text-2xl">
                            Violation
                        </div>
                        <div className="flex">
                            <div className="w-2/3">
                                <p className="font-body font-bold text-m text-base text-pink-600 block pr-4">
                                    Code
                                </p>
                            </div>
                            <div className="w-1/3">
                                <p className="font-body text-m text-base text-pink-600">
                                    {  violationInformation.code }
                                </p>
                            </div>
                        </div>
                        <div>
                            <div className="w-1/3">
                                <p className="font-body font-bold text-m text-base text-pink-600 block pr-4">
                                    Description
                                </p>
                            </div>
                            <div className="w-2/3 pt-1">
                                <p className="font-body text-m text-base text-pink-600">
                                    {  violationInformation.description }
                                </p>
                            </div>
                        </div>
                    </div>
                )
            }
        </div>
    );
};

//
// PropTypes


// Exports
// -------

export default SpecificInformation;
