// Imports
// -------

// Libraries
import React from 'react';

// Internal
// --------

//
// Components

// TODO: Should definitely use props instead of "innerText"
function Info() {
    //
    // Render

    return (
        <div id='info' className='tooltip bg-green-200 p-2 text-m rounded' />
    );
}

// Exports
// -------

export default Info;
