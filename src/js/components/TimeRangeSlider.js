// Imports
// -------

// Libraries
import React, { useCallback, useEffect, useRef, createRef, memo, useContext } from 'react';
import PropTypes from 'prop-types';
import ShowNarrativeContext from '../context/ShowNarrativeContext';

// Internal
// --------

//
// Helpers

// TODO: Would be smart to make this check more robust in the future
function compareProps(prevProps, nextProps) {
    return prevProps.selectedTime === nextProps.selectedTime && prevProps.showNarrative === nextProps.showNarrative;
}

//
// Components

const TimeRangeSlider = memo(({ selectedTime, setSelectedTime, alaskaRestaurantInspectionDataReader, delay }) => {
    //
    // Context

    const {
        showNarrative
    } = useContext(ShowNarrativeContext);

    //
    // References

    const inputRef = useRef(null);
    let timeoutID = createRef();

    //
    // OnMount

    useEffect(() => {
        const inputEl = inputRef.current;
        const value = inputEl.value;
        setSelectedTime(+value);
    }, []);

    useEffect(() => {
        if (selectedTime !== null) {
            const input = inputRef.current;
            input.value = selectedTime.toString();
        }
    }, [selectedTime]);

    //
    // Instance Methods

    const handleChange = useCallback(({ target: { value } }) => {
        if (timeoutID !== null) {
            clearTimeout(timeoutID);
        }

        timeoutID = setTimeout(() => {
            setSelectedTime(+value);
            timeoutID = null;
        }, delay);
    }, []);

    //
    // Render

    const alaskaRestaurantInspectionData = alaskaRestaurantInspectionDataReader();
    const length = alaskaRestaurantInspectionData.inspection_date.length;
    const min = alaskaRestaurantInspectionData.inspection_date[0];
    const max = alaskaRestaurantInspectionData.inspection_date[length - 1];

    return (
        <div>
            <input ref={ inputRef } className='timeRangeSlider' type='range' min={ min } max={ max }
                   onChange={ handleChange } disabled={ showNarrative } />
        </div>
    );
}, compareProps);

//
// PropTypes

TimeRangeSlider.propTypes = {
    delay: PropTypes.number
};

TimeRangeSlider.defaultProps = {
    delay: 100
};

// Exports
// -------

export default TimeRangeSlider;
