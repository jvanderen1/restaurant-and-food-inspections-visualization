// Imports
// -------

// Libraries
import React, { useState, useCallback, useContext } from 'react';
// Context
import ShowNarrativeContext from '../context/ShowNarrativeContext';

// Internal
// --------

//
// Helpers

const information = [
    {
        title: "Welcome!",
        body: "In this Martini Narrative, we will explore various restaurants that have received restaurant inspections in Anchorage, AK.",
        item: 0,
        time: 1460332800000,
    },
    {
        title: "Seward's Folly",
        body: "We begin our journey at Seward's Folly, where a violation has occurred. They appeared to have broken code 41 for storing utensils improperly.",
        item: 0,
        time: 1460332800000
    },
    {
        title: "Ravenwood Elementary Cafeteria",
        body: `We then migrate to Ravenwood Elementary where no violation has occurred. This "restaurant" is spot on!`,
        item: 1374,
        time: 1473811200000,
    },
    {
        title: "China Town Restaurant",
        body: "Next, we move to China Town Restaurant where they received the lowest inspection score, yikes!",
        item: 5788,
        time: 1507680000000,
    },
    {
        title: "Total Violations",
        body: "As can be shown in this dataset, the total number of violations nearly matches the number of restaurants.",
        item: 9999,
        time: 1555891200000,
    },
    {
        title: "Conclusion",
        body: "While a violation is never good, they occur all to often in restaurants. Be careful when picking your next restaurant!",
        item: 9999,
        time: 1555891200000,
    },
    {
        title: "Your Turn",
        body: "To explore more restaurants at a period of time, simply use the slider at the bottom and hover over the restaurant's in the map. You can even zoom and pan across the data! Happy hunting!",
        item: 9999,
        time: 1555891200000,
    }
];

//
// Components

// TODO: Feature idea: embed Google image of address
const Narrative = ({ setSelectedItem, setSelectedTime }) => {
    //
    // State

    const [scene, setScene] = useState(0);

    //
    // Context

    const {
        setShowNarrative
    } = useContext(ShowNarrativeContext);

    //
    // Instance methods

    const setItemAndPosition = useCallback(({ item, time }) => {
        setSelectedItem(item);
        setSelectedTime(time);
    }, []);

    const handleNext = () => {
        const newScene = scene + 1;

        if (newScene < information.length) {
            setItemAndPosition(information[newScene]);
        }

        setScene(() => newScene);
    };

    const handlePrev = () => {
        const newScene = scene - 1;

        if (newScene >= 0) {
            setItemAndPosition(information[newScene]);
        }

        setScene(() => newScene);
    };

    //
    // Render

    // TODO: There's probably a better way than this
    if (scene === information.length) {
        setTimeout(() => setShowNarrative(false), 0);
        return (
            <React.Fragment/>
        );
    }

    const {
        title,
        body,
    } = information[scene];

    return (
        <div className="card bg-yellow-100">
            <div className="px-6 py-4">
                <div className="font-title font-bold text-pink-800 text-base text-xl mb-2">
                    { title }
                </div>
                <div className="font-body text-pink-600 text-s text-base my-5">
                    { body }
                </div>
                <div className="inline-flex">
                    <button className="py-2 px-4 rounded-l" onClick={ handlePrev } disabled={ scene === 0 }>
                        Prev
                    </button>
                    <button
                        className="py-2 px-4 rounded-r" onClick={ handleNext }>
                        { (scene < information.length - 1) ? 'Next' : 'Done' }
                    </button>
                </div>
            </div>
        </div>
    );
};

//
// PropTypes


// Exports
// -------

export default Narrative;
