// Imports
// -------

// Libraries
import React from 'react';
import ReactDOM from 'react-dom';
// Components
import App from './js/components/App'
import './css/styles.css'


const root = document.getElementById('root');
ReactDOM.render(<App/>, root);
