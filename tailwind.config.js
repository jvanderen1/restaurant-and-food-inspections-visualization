module.exports = {
    purge: {
        content: [
            './src/**/*.js',
            './src/**/*.html'
        ]
    },
    theme: {
        extend: {
            fontFamily: {
                'title': ['Raleway'],
                'body': ['Open Sans']
            }
        }
    },
    variants: {},
    plugins: []
};
